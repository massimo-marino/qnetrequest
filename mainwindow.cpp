#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);

  QObject::connect(&timer_, SIGNAL(timeout()), this, SLOT(updateIfaceList()));
  timer_.start(1000);

  QObject::connect(ui->contactServerPushButton, SIGNAL(clicked()), this, SLOT(contactServer()), Qt::QueuedConnection);

  QStringList wordList;
  wordList << "alpha" << "Omega" << "Bravo" << "omicron" << "zeta";

  QLineEdit *lineEdit = new QLineEdit(this);

  QCompleter *completer = new QCompleter(wordList, this);
  completer->setCaseSensitivity(Qt::CaseInsensitive);
//  completer->setCompletionMode(QCompleter::PopupCompletion);
//  completer->setCompletionMode(QCompleter::InlineCompletion);
  completer->setCompletionMode(QCompleter::UnfilteredPopupCompletion);
  lineEdit->setCompleter(completer);
  lineEdit->setGeometry(50, 150, 200, 30);
  connect(lineEdit, SIGNAL(textChanged(const QString&)), this, SLOT(foo(const QString&)));
}

void MainWindow::foo(const QString& text)
{
  qDebug() << "Text changed: " << text;
}

void MainWindow::contactServer()
{
  if ( !networkAvailable_ )
  {
    qWarning() << "network NOT available";
    return;
  }

  QNetworkAccessManager manager;
  QEventLoop evl;
  // timer for the request
  QTimer tT;
  // timer for calling QApplication::processEvents() in the event loop
  QTimer tT2;

  tT.setSingleShot(true);
  tT2.setSingleShot(false);

  QObject::connect(&tT2,     SIGNAL(timeout()),                this, SLOT(pe()), Qt::QueuedConnection);
  QObject::connect(&tT,      SIGNAL(timeout()),                &evl, SLOT(quit()), Qt::QueuedConnection);
  QObject::connect(&manager, SIGNAL(finished(QNetworkReply*)), &evl, SLOT(quit()), Qt::QueuedConnection);
  QObject::connect(&manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(finishedDownload(QNetworkReply*)), Qt::QueuedConnection);

  // network error because of 'host not found'
  QNetworkReply *reply = manager.get(QNetworkRequest(QUrl("https://www.qtcentrexxxxxxxxxxxxxxxx.org")));

  // ok
  //QNetworkReply *reply = manager.get(QNetworkRequest(QUrl("https://www.qtcentre.org")));

  // ok
  //QNetworkReply *reply = manager.get(QNetworkRequest(QUrl("https://doc.qt.io/archives/qq/qq27-responsive-guis.html")));

  // log network error
  QObject::connect(reply, QOverload<QNetworkReply::NetworkError>::of(&QNetworkReply::error),
      [=](QNetworkReply::NetworkError code)
      {
        qWarning() << "network reply error [error code =" << code
                   << "] reason:"
                   << reply->errorString();
      });

  tT.start(10000); // timeout in msecs for network request
  tT2.start(100);  // timeout in msecs for calling QApplication::processEvents()

  evl.exec();

  tT2.stop();

  if(tT.isActive())
  {
    // request completed
    tT.stop();
    qInfo() << "MainWindow::MainWindow: request completed";
  }
  else
  {
    // request timed-out
    qWarning() << "MainWindow::MainWindow: request timed-out";
  }
}

// slot: call QApplication::processEvents() at every time-out of tT2
void MainWindow::pe()
{
  qDebug() << "PE-";
  QApplication::processEvents();
}

// slot
void MainWindow::finishedDownload(QNetworkReply* reply)
{
  auto allData {reply->readAll()};
  auto rhl {reply->rawHeaderList()};
  auto sh {reply->header(QNetworkRequest::ServerHeader)};
  auto uah {reply->header(QNetworkRequest::UserAgentHeader)};
  auto cdh {reply->header(QNetworkRequest::ContentDispositionHeader)};
  auto clh {reply->header(QNetworkRequest::ContentLengthHeader)};
  auto lh {reply->header(QNetworkRequest::LocationHeader)};
  auto cth {reply->header(QNetworkRequest::ContentTypeHeader)};
  auto lmh {reply->header(QNetworkRequest::LastModifiedHeader)};
  auto ch {reply->header(QNetworkRequest::CookieHeader)};
  auto sch {reply->header(QNetworkRequest::SetCookieHeader)};

  qInfo() << "MainWindow::finishedDownload: download completed";

  qInfo() << "LocationHeader:" << lh.toString();
  qInfo() << "ServerHeader:" << sh.toString();
  qInfo() << "ContentTypeHeader:" << cth.toString();
  qInfo() << "ContentLengthHeader:" << clh.toString();

  if ( uah.isValid() )
  {
    qInfo() << "UserAgentHeader:" << uah.toString();
  }
  else
  {
    qInfo() << "UserAgentHeader: INVALID";
  }

  if ( cdh.isValid() )
  {
    qInfo() << "ContentDispositionHeader: " << cdh.toString();
  }
  else
  {
    qInfo() << "ContentDispositionHeader: INVALID";
  }

  if ( lmh.isValid() )
  {
    qInfo() << "LastModifiedHeader: " << lmh.toString();
  }
  else
  {
    qInfo() << "LastModifiedHeader: INVALID";
  }

  if ( ch.isValid() )
  {
    qInfo() << "CookieHeader:" << ch.toString();
  }
  else
  {
    qInfo() << "CookieHeader: INVALID";
  }

  if ( sch.isValid() )
  {
    qInfo() << "SetCookieHeader:" << sch.toString();
  }
  else
  {
    qInfo() << "SetCookieHeader: INVALID";
  }
  // headers
  for (auto& e : rhl)
  {
    qInfo("'%s'", e.data());
  }
  // all response data
  qInfo("All data (%d bytes): '%s'", allData.size(), allData.data());
}

void
MainWindow::updateIfaceList()
{
  QList<QNetworkInterface> ifaces {QNetworkInterface::allInterfaces()};
  QList<QNetworkInterface> validIfs {};

  if ( false == ifaces.isEmpty() )
  {
    for (const auto& iface : ifaces)
    {
      QString iname {iface.name()};
      QNetworkInterface::InterfaceType iftype {iface.type()};
      unsigned int flags {iface.flags()};
      bool isLoopback {static_cast<bool>(flags & QNetworkInterface::IsLoopBack)};
      bool isP2P      {static_cast<bool>(flags & QNetworkInterface::IsPointToPoint)};
      bool isRunning  {static_cast<bool>(flags & QNetworkInterface::IsRunning)};
      bool isUp       {static_cast<bool>(flags & QNetworkInterface::IsUp)};
      bool isValid    {iface.isValid()};

      if ( isValid && isRunning && isUp && !isLoopback && !isP2P )
      {
        switch (iftype)
        {
          case QNetworkInterface::Wifi:
          case QNetworkInterface::Ethernet:
          case QNetworkInterface::Fddi:
          case QNetworkInterface::Ieee80216:
          case QNetworkInterface::Ieee1394:
          case QNetworkInterface::Ieee802154:
          case QNetworkInterface::SixLoWPAN:
          {
            validIfs.push_back(iface);
          }
          break;

          default:
          break;
        }
      }
    }
  }
  if ( 0 == validIfs.size() )
  {
    qWarning() << "network DOWN";
    networkAvailable_ = false;
  }
  else
  {
    for (const auto& iface : validIfs)
    {
      qDebug() << "if name:" << iface.name()
               << ((iface.isValid()) ? "VALID" : "NOT VALID")
               << " RUNNING UP - if type: " << iface.type();
    }
    networkAvailable_ = true;
  }
  emit networkInterfacesStatusUpdated();
}

MainWindow::~MainWindow()
{
  delete ui;
}
