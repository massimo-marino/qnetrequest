#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLineEdit>
#include <QCompleter>
#include <QNetworkInterface>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QEventLoop>
#include <QTimer>
#include <QtDebug>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

private:
  Ui::MainWindow *ui;

  QTimer timer_;
  bool networkAvailable_ {false};

signals:
  void networkInterfacesStatusUpdated();

public slots:
  void contactServer();
  void finishedDownload(QNetworkReply* reply);
  void pe();
  void updateIfaceList();
  void foo(const QString&);
};

#endif // MAINWINDOW_H
